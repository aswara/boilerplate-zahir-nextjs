export const isClient = process.browser;

export { default as getSafe } from './getSafe';
export { default as urlCrypt } from './urlCrypt';
export { default as localStorage } from './localstorage';