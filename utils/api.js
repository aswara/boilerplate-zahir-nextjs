import axios from 'axios';
import moment from 'moment';

import cookies from 'js-cookie';
import config from '../config';

const base64 = require('base-64');

const secret = `${config.CLIENT_ID}:${moment().format('MMYY')}${config.CLIENT_SECRET}`;
const client = base64.encode(secret);


export function getAuth() {
    const accessToken = cookies.get('access_token');
    const slug = cookies.get('slug');
    if (accessToken) {
        setHeader({ accessToken, slug })
    }
}

export function setHeader({ accessToken, slug }) {

    const tokenType = 'Bearer';
    axios.defaults.headers.common['Content-Type'] = 'application/json';

    if (accessToken) {
        axios.defaults.headers.common['Authorization'] = `${tokenType} ${accessToken}`;
    }

    if (slug) {
        axios.defaults.headers.common['slug'] = slug;
    }
}

export function setAuth({ accessToken }) {
    cookies.set('access_token', accessToken)
    setHeader({ accessToken })
}


export function accessToken() {
    const url = '/api/v2/me'
    const auth = {
        username: config.username,
        password: config.password
    }
    return axios({
        url,
        method: 'GET',
        auth,
        headers: {
            'Content-Type': 'application/json',
            'client': client
        }
    })
}

export function checkEmail(email) {
    return new Promise((resolve, reject) => {
        accessToken()
            .then(res => {
                const access_token = res.data.access_token;
                setAuth({ accessToken: access_token });
                axios.post('/api/v2/zahir_id/api/is-exist-user', { email, access_token })
                    .then(res => {
                        resolve(res)
                    })
            })
            .catch(err => {
                reject(err)
            })
    })
}

export function login({ email, password }) {
    return new Promise((resolve, reject) => {
        const url = '/api/v2/me'
        const auth = {
            username: email,
            password: password
        }
        axios({
            url,
            method: 'GET',
            auth,
            headers: {
                'Content-Type': 'application/json',
                'client': client
            }
        }).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}

export function migrate() {
    return axios.post(`/api/v2/migration`)
}

export function register(values) {
    const url = `/api/v2/registrations`
    let first_name = '';
    let last_name = '';

    const {
        name,
        email,
        password,
        phone,
        business,
        language,
        city,
        currency,
        address
    } = values

    if (name) {
        const split = name.split(" ");
        first_name = split[0];
        if (split.length >= 2) {
            split.shift();
            last_name = split.join(' ')
        }
    }

    const data = {
        "user": {
            "email": email,
            "password": password,
            "first_name": first_name,
            "last_name": last_name,
            "mobile_phone": phone,
            "client": client
        },
        "company": {
            "name": name,
            "address": address,
            "address_1": null,
            "address_2": null,
            "city": city,
            "province": null,
            "country_code": 'id',
            "country_name": null,
            "postal_code": null,
            "phone": phone,
            "email": email ? email : null,
            "website": ""
        },
        "setup": {
            "business_id": business && business.id ? business.id : null,
            "business_name": business && business.name ? business.name : null,
            "business_type": business && business.type ? business.type.id : null,
            "business_type_name": business && business.type ? business.type.name : null,
            "is_use_default_coa": true,
            "currency_code": currency.code,
            "language_code": language,
            "tax_code": 'id',
            "period": {
                "start": dayjs().month() + 1,
                "end": 12,
                "year": dayjs().year()
            }
        },
        "membership": {
            "variant_id": 4
        },
        "config": {
            "sent_email": false
        }
    }

    return axios.post(url, data)
}