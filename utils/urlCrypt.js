const urlCrypt = require('url-crypt')('zahironline-simplyinvoice-cashmanager-posx-1234567890');

function crypt(data) {
    let base64 = urlCrypt.cryptObj(data);
    return base64;
}

function decrypt(data) {
    let value = urlCrypt.decryptObj(data);
    return value;
}

export default {
    crypt,
    decrypt
}