export function save(key, value) {
    try {
        const data = JSON.stringify(value)
        window.localStorage.setItem(key, data)
    } catch {
        console.warn('failed save local storage')
    }
}

export function get(key) {
    try {
        const data = window.localStorage.getItem(key)
        return JSON.parse(data)
    } catch {
        return null
    } 
}

export function remove(key) {
    try {
        window.localStorage.removeItem(key)
    } catch {
        return null
    } 
}

export default {
    save,
    get,
    remove
}