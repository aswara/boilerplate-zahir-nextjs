import React from 'react';


function Layout(props) {

  return (
    <div className="page-layout">
      {props.children}
      <style jsx global>{`
            * {
                font-family: 'Muli', sans-serif;
     
            }
            body { 
                padding: 0;
                margin: 0;
                color: #394D6F;
            }
            a {
              text-decoration: none;
            }

            footer {
                text-align: center;
                padding: 30px;
            }

        `}</style>
    </div>
  )
}

export default Layout