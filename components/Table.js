import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import Paper from '@material-ui/core/Paper';
import Skeleton from '@material-ui/lab/Skeleton';

import { AutoSizer, Column, Table, InfiniteLoader } from 'react-virtualized';
import { getSafe, isClient } from '../utils';

const styles = theme => ({
    '@keyframes slideCell': {
        '0%': { marginTop: 20 },
        '100%': { marginTop: 0 }
    },
    flexContainer: {
        display: 'flex',
        alignItems: 'center',
        boxSizing: 'border-box',
    },
    tableRow: {
        cursor: 'pointer',
    },
    tableRowHover: {
        animation: 'slideCell 0.5s',
        cursor: 'pointer',
        border: 'none',
        '&:hover': {
            backgroundColor: theme.palette.grey[200],
        },
    },
    tableCell: {
        flex: 1,
        border: 'none',
        fontSize: 12
    },
    noClick: {
        cursor: 'initial',
    },
    header: {
        fontWeight: 600,
        borderBottom: '1px solid #D1D8E0',
        color: '#A5B1C2'
    },
    stripe: {
        backgroundColor: '#FCFCFC'
    }
});

class MuiVirtualizedTable extends React.PureComponent {
    static defaultProps = {
        headerHeight: 48,
        rowHeight: 48,
        height: 500
    };

    getRowClassName = ({ index }) => {
        const { classes, onRowClick } = this.props;
        const stripe = index % 2 === 0
        return clsx(classes.tableRow, classes.flexContainer, {
            [classes.tableRowHover]: index !== -1,
            [classes.stripe]: stripe
        });
    };

    cellRenderer = ({ cellData, columnIndex, rowData }) => {
        const { columns, classes, rowHeight, onRowClick, response, data } = this.props;
        const loading = response.loadingGet && data.length === 0;

        // loading row
        if(rowData && rowData.type === 'loading') {
            return (
                <TableCell
                    component="div"
                    className={clsx(classes.tableCell, classes.flexContainer, {
                    })}
                    variant="body"
                    style={{ height: rowHeight }}
                    align={(columnIndex != null && columns[columnIndex].numeric) || false ? 'right' : 'left'}
                >
                    <Skeleton width={'100%'} variant="text" />
                </TableCell>
            );
        }

        return (
            <TableCell
                component="div"
                className={clsx(classes.tableCell, classes.flexContainer, {
                })}
                variant="body"
                style={{ height: rowHeight }}
                align={(columnIndex != null && columns[columnIndex].numeric) || false ? 'right' : 'left'}
            >
                {loading ? <Skeleton width={'100%'} variant="text" /> : cellData}
            </TableCell>
        );
    };

    headerRenderer = ({ label, columnIndex }) => {
        const { headerHeight, columns, classes } = this.props;

        return (
            <TableCell
                component="div"
                className={clsx(classes.tableCell, classes.flexContainer, classes.noClick, classes.header)}
                variant="head"
                style={{ height: headerHeight }}
                align={columns[columnIndex].numeric || false ? 'right' : 'left'}
            >
                <span>{label}</span>
            </TableCell>
        );
    };

    loadMoreRow = () => {
        if (this.props.loadmore) {
            this.props.loadmore()
        }
    }

    handleClick = (data) => {
        const { onSelect } = this.props;
        const { rowData } = data;
        if (onSelect) {
            onSelect(rowData)
        }
    }

    getLastRow = (data) => {
        return data
      }
    

    render() {
        const {
            classes,
            data,
            columns,
            rowHeight,
            headerHeight,
            response,
            height
        } = this.props;
        let rows = data || []
        const loading = response.loadingGet;

        if (loading && rows.length === 0) {
            rows = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        }
        
        if (loading && rows.length > 0) {
            const rowLoading = {
                type: "loading"
            } 
            rows.push(rowLoading)
            rows.push(rowLoading)
            rows.push(rowLoading)
        }

        return (
            <Paper elevation={0} style={{ height, width: '100%' }}>
                <InfiniteLoader
                    isRowLoaded={({ index }) => rows[index]}
                    loadMoreRows={this.loadMoreRow}
                    rowCount={getSafe(() => response.data.count)}
                    threshold={1}
                >
                    {({ onRowsRendered, registerChild }) => (
                        <AutoSizer>
                            {({ height, width }) => (
                                <Table
                                    ref={registerChild}
                                    onRowsRendered={onRowsRendered}
                                    height={height}
                                    width={width}
                                    rowHeight={rowHeight}
                                    headerHeight={headerHeight}
                                    rowCount={rows.length}
                                    rowGetter={({ index }) =>  rows[index]}
                                    rowClassName={this.getRowClassName}
                                    gridStyle={{ outline: 'none' }}
                                    onRowClick={this.handleClick}
                                >
                                    {columns.map(({ dataKey, ...other }, index) => {
                                        return (
                                            <Column
                                                key={dataKey}
                                                headerRenderer={headerProps =>
                                                    this.headerRenderer({
                                                        ...headerProps,
                                                        columnIndex: index,
                                                    })
                                                }
                                                className={classes.flexContainer}
                                                cellRenderer={this.cellRenderer}
                                                dataKey={dataKey}
                                                {...other}
                                            />
                                        );
                                    })}
                                </Table>
                            )}
                        </AutoSizer>
                    )}
                </InfiniteLoader>
            </Paper>
        );
    }
}

MuiVirtualizedTable.propTypes = {
    classes: PropTypes.object.isRequired,
    columns: PropTypes.arrayOf(
        PropTypes.shape({
            dataKey: PropTypes.string.isRequired,
            label: PropTypes.string.isRequired,
            numeric: PropTypes.bool,
            width: PropTypes.number.isRequired,
        }),
    ).isRequired,
    headerHeight: PropTypes.number,
    onRowClick: PropTypes.func,
    rowHeight: PropTypes.number,
};

export default withStyles(styles)(MuiVirtualizedTable);
