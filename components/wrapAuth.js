import React,  { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Language from './Language';
import Link from 'next/link';
import Footer from './Footer';
import LinearProgress from '@material-ui/core/LinearProgress';

import useUser from '../lib/useUser';

const WrapAuth = (props) => {
    const { children, full } = props;

    const { setting } = useUser();

    return (
        <div>
           { setting.loading && <LinearProgress /> }
         <Grid container justify="center">
            <Grid style={{ marginTop: 10 }} md={12} sm={12} xs={11} item>
                <Grid className="header" container justify="space-between">
                    <Grid style={{ marginTop: 5 }} item>
                        <Link href='/'>
                            <a>
                                <img className="logo" src="static/logoblue.svg"  alt="logo"/>
                            </a>
                        </Link>
                        
                    </Grid>
                    <Grid item>
                        <div className="language">
                            <Language color="#394D6F" />
                        </div>
                    </Grid>
                </Grid>
            </Grid>
            <Grid style={{ marginTop: full ? 0 : 60 }} xs={11} sm={full ? 12 : 6} md={ full ? 8 : 4} xl={ full ? 7 : 4} item>
                {children}
            </Grid>
 
            <Grid item xs={12}>
                <Footer />
            </Grid>
            

        </Grid>
        <style jsx>{`
            .logo {
                width: 225px;
                margin-left: 20px
            }
            .language {
                margin-right: 10px
            }
            @media only screen and (max-width: 600px) {
                .logo {
                    width: 200px;
                    margin-left: 10px;
                }
                .language {
                    margin-right: 0px
                }
            }
        `}</style>
        </div>
    );
};

export default WrapAuth;