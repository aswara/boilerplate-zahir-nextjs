import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ArrowDropDown from '@material-ui/icons/ArrowDropDownRounded';


import { useTranslation } from 'react-i18next';


const StyledMenu = withStyles({
    paper: {
        border: 'none',
        borderRadius: 5
    },
})(props => (
    <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
        }}
        {...props}
    />
));

const StyledMenuItem = withStyles(theme => ({
    root: {
        fontSize: 14,
        '&:focus': {
            backgroundColor: theme.palette.primary.main,
            '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
                color: theme.palette.common.white,
            },
        },
    },
}))(MenuItem);


const FlagEn = (props) => {
    const {
        style,
        color,
        size,
    } = props
    return (
        <svg width={size} height={size} style={{ color, ...style }} viewBox="0 0 17 15" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M15 2.00002C15 0.895424 14.1046 0 13 0H1.99995C0.895426 0 0 0.895424 0 2.00002V13C0 14.1046 0.895426 15 1.99995 15H13C14.1046 15 15 14.1046 15 13V2.00002Z" fill="#41479B" />
            <path fill-rule="evenodd" clip-rule="evenodd" d="M7.5 6.73704L14.9432 1.52521C14.9804 1.67738 15 1.83646 15 2.00003V3.01148L8.5896 7.50001L15 11.9886V13.0001C15 13.1636 14.9804 13.3227 14.9432 13.4749L7.5 8.26306L0.0567009 13.4749C0.0196509 13.3227 0 13.1636 0 13.0001V11.9886L6.41033 7.50001L0 3.01148V2.00003C0 1.83646 0.0196509 1.67738 0.0567009 1.52521L7.5 6.73704V6.73704Z" fill="#FF4B55" />
            <path fill-rule="evenodd" clip-rule="evenodd" d="M14.9846 13.2495C14.9658 13.4005 14.9302 13.5462 14.8797 13.6847L7.5 8.51747L0.120225 13.6847C0.0697498 13.5462 0.0341263 13.4005 0.0153763 13.2495L7.5 8.00882L14.9846 13.2495ZM15 2.75724V3.26589L8.95283 7.50009L15 11.7344V12.243L8.27715 7.53564V7.46462L15 2.75724V2.75724ZM0 2.75724L6.72285 7.46462V7.53564L0 12.243V11.7344L6.0471 7.50009L0 3.26589V2.75724V2.75724ZM14.8797 1.31544C14.9302 1.45404 14.9658 1.59977 14.9846 1.75074L7.5 6.99144L0.0153763 1.75074C0.0342013 1.59977 0.0697498 1.45404 0.120225 1.31544L7.5 6.4828L14.8797 1.31544Z" fill="white" />
            <path fill-rule="evenodd" clip-rule="evenodd" d="M8.74995 6.25005H15V8.75002H8.74995V15H6.24998V8.75002H0V6.25005H6.24998V0H8.74995V6.25005V6.25005Z" fill="#FF4B55" />
            <path fill-rule="evenodd" clip-rule="evenodd" d="M0 8.95838V8.54167H6.45833V15H6.04163V8.95838H0ZM15 8.54167V8.95838H8.9583V15H8.54168V8.54167H15V8.54167ZM8.54168 0H8.9583V6.0417H15V6.4584H8.54168V0V0ZM6.04163 0H6.45833V6.4584H0V6.0417H6.04163V0V0Z" fill="white" />
        </svg>
    )
}

const FlagId = (props) => {
    const {
        style,
        color,
        size,
    } = props
    return (
        <svg width={size} height={size} style={{ color, ...style }} viewBox="0 0 17 15" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M15 2.00002C15 0.895424 14.1046 0 13 0H2.00002C0.895424 0 0 0.895424 0 2.00002V13C0 14.1046 0.895424 15 2.00002 15H13C14.1046 15 15 14.1046 15 13V2.00002V2.00002Z" fill="white" />
            <path d="M15 2.00002C15 1.46962 14.7893 0.960903 14.4143 0.585828C14.0392 0.210753 13.5304 0 13 0C10.1292 0 4.8708 0 2.00002 0C1.46955 0 0.960823 0.210753 0.585823 0.585828C0.210748 0.960903 0 1.46962 0 2.00002C0 4.22182 0 7.5 0 7.5H15V2.00002V2.00002Z" fill="#FF4B55" />
        </svg>

    )
}

export default function CustomizedMenus(props) {
    const { color } = props;
    const { i18n } = useTranslation()
    const [anchorEl, setAnchorEl] = React.useState(null);

    function handleClick(event) {
        setAnchorEl(event.currentTarget);
    }

    function handleClose() {
        setAnchorEl(null);
    }

    function handleChange(lang) {
        i18n.changeLanguage(lang)
        handleClose()
    }

    return (
        <div>
            <Button
                style={{ color: 'white', textTransform: 'capitalize' }} P
                aria-controls="customized-menu"
                aria-haspopup="true"
                onClick={handleClick}
            >
                {i18n.language === 'id' ?
                    <Fragment><FlagId size={22} style={{ marginRight: 5 }} /></Fragment> :
                    <Fragment><FlagEn size={22} style={{ marginRight: 5 }} /></Fragment>}
                <ArrowDropDown style={{ transition: '0.2s', color: color, transform: anchorEl ? 'rotate(180deg)' : 'rotate(0deg)' }} />
            </Button>
            <StyledMenu
                id="customized-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <StyledMenuItem
                    onClick={() => handleChange('en')}
                >
                    <ListItemIcon>
                        <FlagEn />
                    </ListItemIcon>
                    <ListItemText primary="English" />
                </StyledMenuItem>
                <StyledMenuItem
                    onClick={() => handleChange('id')}
                >
                    <ListItemIcon>
                        <FlagId />
                    </ListItemIcon>
                    <ListItemText primary="Indonesia" />
                </StyledMenuItem>
            </StyledMenu>
        </div>
    );
}
