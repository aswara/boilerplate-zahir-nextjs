import React, { useState, useEffect } from 'react';
import Select from 'react-select';
import ArrowDown from '@material-ui/icons/ArrowDropDownRounded';
import { useTheme } from '@material-ui/styles';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import useFetch from '../../lib/useFetch';
import Grid from '@material-ui/core/Grid';
import Check from '@material-ui/icons/CheckRounded';

function variantStyle(type, error) {
    const theme = useTheme();

    switch (type) {
        case 'filled':
            return {
                control: (base, { isFocused }) => ({
                    ...base,
                    border: `1px solid ${ error ? theme.palette.error.light : 'transparent'}`,
                    backgroundColor: isFocused ? 'rgba(0, 0, 0, 0.09)' : '#F5F7F8',
                    borderRaduis: 5,
                    boxShadow: "none",
                    padding: 10,
                    '&:hover': {
                        backgroundColor: isFocused ? 'rgba(0, 0, 0, 0.09)' : 'rgba(0, 0, 0, 0.13)',
                    },
                    transition: 'all 0.5s ease 0s',
                }),
                indicatorSeparator: (base) => ({
                    ...base,
                    backgroundColor: 'transparent',
                }),
                singleValue: (base) => ({
                    ...base,
                    padding: 0
                }),
                placeholder: (base) => ({
                    ...base,
                    color: '#A5B1C2'
                })
            }
            break;
        default:
            return {
                control: (base, { }) => ({
                    ...base,
                    backgroundColor: 'transparent',
                    border: 'none',
                    boxShadow: "none",
                    marginLeft: -8,
                    marginRight: -8
                }),
                indicatorSeparator: (base) => ({
                    ...base,
                    backgroundColor: 'transparent',
                }),
                singleValue: (base) => ({
                    ...base,
                    padding: 0
                }),
                placeholder: (base) => ({
                    ...base,
                    color: '#A5B1C2'
                })
            }
            break;
    }
}


function SelectComponent(props) {
    const {
        onChange,
        value,
        options,
        keyLabel,
        variant,
        placeholder,
        url,
        errors,
        touched,
        name,
        onBlur
    } = props

    function formatOption(params) {
        return params.map((item, index) => ({
            value: item,
            label:
                <Grid container justify="space-between">
                    <Grid item>
                        {item[keyLabel]}
                    </Grid>
                    <Grid item>
                        {value && (value[keyLabel] === item[keyLabel]) ? <Check color="primary" fontSize="small" /> : null }
                    </Grid>
                </Grid>

        }));
    }

    const option = formatOption(options)

    const [open, setOpen] = useState(false);
    const [datas, setDatas] = useState(url ? option : options);
    

    const { values, get, results, loadmore, search, response, autoLoad } = useFetch(url)


    useEffect(() => {
        if(url) {
            setDatas(formatOption(results || []))
        }
    }, [results])


    const DropdownIndicator = () => {
        return (
            <div style={{ width: 35 }}
            >
                <ArrowDown
                    fontSize="large"
                    color="primary"
                    style={{
                        color: variant === 'filled' ? '#D1D8E0' : false,
                        marginBottom: -3,
                        transition: 'all 0.5s ease 0s',
                        transform: open ? 'rotate(180deg)' : 'rotate(0deg)',
                    }}
                />
            </div>
        );
    }

    function getData() {
        if (url) {
            get()
        }
    }

    function handleLoadMore(e) {
        loadmore();
    }

    function handleSearch(val, { action }) {
        if (url && action === 'input-change') {

            actionSearch(val);
        }
    }

    let timeout = null

    function actionSearch(val) {
        if (timeout) clearTimeout(timeout);
        timeout = setTimeout(() => {
            if (val === '') {
                get();
            } else {
                search(val)
            }
        }, 500);
    }


    const error = errors && errors[name] && touched[name] && errors[name][keyLabel];

    if(!url) {
        return (
            <FormControl fullWidth>
                <Select
                    name={name}
                    value={value && { value, label: value[keyLabel] || '' }}
                    onChange={onChange}
                    options={datas}
                    maxMenuHeight={200}
                    onBlur={onBlur}
                    onMenuOpen={() => setOpen(true)}
                    onMenuClose={() => setOpen(false)}
                    menuPlacement="auto"
                    components={{ DropdownIndicator }}
                    styles={variantStyle(variant, error)}
                    placeholder={placeholder}
                />
                { error && <FormHelperText error>{error}</FormHelperText> }
            </FormControl>
        )
    }

    return (
        <FormControl fullWidth>
            <Select
                name={name}
                value={value && { value, label: value[keyLabel] || '' }}
                onChange={onChange}
                options={datas}
                maxMenuHeight={200}
                onFocus={getData}
                onBlur={onBlur}
                onMenuOpen={() => setOpen(true)}
                onMenuClose={() => setOpen(false)}
                menuPlacement="auto"
                components={{ DropdownIndicator }}
                styles={variantStyle(variant, error)}
                placeholder={placeholder}
                onMenuScrollToBottom={(e) => { 
                    loadmore() 
                }}

                onInputChange={handleSearch}
                filterOption={url ? data => data : false}
            />
            { error && <FormHelperText error>{error}</FormHelperText> }
        </FormControl>

    )
}

SelectComponent.defaultProps = {
    options: [],
    variant: 'text',
    keyLabel: 'name'
}

export default SelectComponent;