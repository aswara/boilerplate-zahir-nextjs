import React,  { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import { useTheme } from '@material-ui/styles';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';

const Input = (props) => {
    const { 
        label, 
        name, 
        values, 
        errors, 
        touched,
    } = props;
    const theme = useTheme();
    const error = errors[name] && touched[name] &&  errors[name]; 
    const [ password, setPassword ] = useState(name === 'password' ? true : false);

    return (
        <div>
            { label &&  <div style={{ marginBottom: 10, color: '#778CA3' }}>{label}</div> }
            <TextField
                variant="filled"
                InputProps={{
                    disableUnderline: true,
                    style: {
                        borderRadius: 5,
                        overflow: 'hidden',
                        border: `1px solid ${ error ? theme.palette.error.light : 'transparent'}`,
                        transition: '0.5s'
                    },
                    endAdornment: <> {
                        name === 'password' &&
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={() => setPassword(!password)}
                          >
                            {!password ? <Visibility style={{ color: '#D1D8E0', fontSize: 20 }} /> : <VisibilityOff style={{ color: '#D1D8E0', fontSize: 20 }} />}
                          </IconButton>
                        </InputAdornment> 
                     } </>
                }}
                type={password ? 'password' : 'text'}
                {...props}
                label={false}
                value={values[name]}
                error={error}
                helperText={error}
            />
        </div>
    );
};

export default Input;