import React from 'react'
import version from '../version';

const Footer = (props) => {
    const { 
        logo
    } = props
    return(
        <footer>
            { logo && <div className="powered">Powered by <a href="https://go.zahironline.com"><img src="/static/zahir.png"  alt="zahir" /></a> </div> } 
            <p>{version} ©2019 <a href="https://zahiraccounting.com">PT. Zahir Internasional</a></p>
            <style jsx>{`
                .powered {
                    font-size: 20px;
                    font-weight: 600;
                }

                img {
                    margin-bottom: -7px;
                }

                a {
                    color: #394D6F;
                }

                p {
                    font-size: 14px;
                }
            `}</style>
        </footer>
)}

export default Footer
