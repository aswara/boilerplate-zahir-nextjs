import { createMuiTheme } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#2D98DA',
    },
    secondary: {
      main: '#20BF6B',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#fff',
    },
    text: {
      primary: '#394D6F'
    }
  },
  typography: {
    fontFamily: 'Muli, sans-serif'
  },
  overrides: {
    MuiButton: {
      root: {
        fontWeight: 600,
        fontSize: '1.175rem',
        textTransform: 'none',
        boxShadow: 'none'
      },
      contained: {
        boxShadow: 'none'
      },
      outlined: {
        borderRadius: 5
      },
      text: {
        padding: '8px 20px'
      },
      sizeLarge: {
        padding: '12px 24px'
      },
      containedSecondary: {
        color: 'white'
      }
    },
    MuiTypography: {
      subtitle1: {
        fontWeight: 'bold'
      },
      h1: {
        fontWeight: 'bold'
      },
      h2: {
        fontWeight: 'bold'
      },
      h3: {
        fontWeight: 'bold'
      },
      h4: {
        fontWeight: 'bold'
      },
      h5: {
        fontWeight: 'bold'
      },
      h6: {
        fontWeight: 'bold'
      },
    },
    MuiListItemIcon: {
      root: {
        minWidth: 25
      }
    },
    MuiCheckbox: {
      root: {
        padding: 0
      }
    },
    MuiFilledInput: {
      input: {
        padding: 17
      },
      root: {
        backgroundColor: '#F5F7F8',
        borderRadius: 5
      },
      adornedEnd: {
        paddingRight: 5
      }
    },

    MuiOutlinedInput: {
      root: {
        border: 'none'
      }
    },
    MuiDivider: {
      root: {
        backgroundColor: '#A5B1C2'
      }
    },
    MuiFormHelperText: {
      contained: {
        marginLeft: 0
      }
    },
    MuiPaper: {
      elevation1: {
        boxShadow: '0px 7px 64px rgba(0, 0, 0, 0.07)'
      }
    },
    MuiDrawer: {
      paper: {
        width: 220,
        overflowY: 'inherit',
        borderRight: '1px solid #E5E5E5',

      },
    }
  }
});

export default theme;