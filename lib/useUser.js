import React,{ createContext, useState, useEffect } from 'react';
import { localStorage, getSafe } from '../utils';
import cookies from 'js-cookie';
import axios from 'axios';
import Router from 'next/router';

import useNotif from './useNotif';
import { setAuth, migrate } from '../utils/api';

const UserContext = createContext();

export default function useUser() {
    return React.useContext(UserContext);
}

export function UserProvider(props) {
    const initialSetting = { drawer: false, drawerSetting: false, loading: false }
    const initialCurrency = {}
    // state
    const [user, setUser] = useState(null);
    const [company, setCompany] = useState(null);
    const [companySettings, setCompanySettings] = useState(null);
    const [setting, setSetting] = useState(initialSetting);
    const [currency, setCurrency] = useState(initialCurrency);

    const { responseError } = useNotif();
    // get data from localstorage
    useEffect(() => {
        const user = localStorage.get('user');
        const company = localStorage.get('company');
        const companySettings = localStorage.get('company_settings');
        const currency = localStorage.get('currency');

        setUser(user);
        setCompany(company);
        setCompanySettings(companySettings);
        setCurrency(currency)
    }, [])


    function logout() {
        localStorage.remove('company');
        localStorage.remove('user');
        localStorage.remove('company_settings');
        localStorage.remove('setting');
        localStorage.remove('invoice');
        localStorage.remove('currency');

        cookies.remove('access_token');
        cookies.remove('slug');
        Router.push({
            pathname: '/login'
        })
    }

    function saveUser(value) {
        setUser(value);
        localStorage.save('user', value);
        const accessToken = value.access_token;
        if(accessToken) {
            setAuth({ accessToken })
        }
    }

    function saveCompany(value) {
        return new Promise( (resolve, reject) => {
            setCompany(value)
            localStorage.save('company', value);
            setSetting(set => ({ ...set, loading: true }))
            const slug = value && value.slug;
            if (slug) {
                axios.defaults.headers.common['slug'] = slug;
                cookies.set('slug', slug);
            }

            // migrate
            migrateDatabase()
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                responseError(err)
                reject(err)
                setSetting(set => ({ ...set, loading: false }))
            })

        })
    }

    function fetchCompanySetting() {
        axios.get(`/api/v2/company_settings?is_skip_pagination=true`)
        .then(res => {
          const data = getSafe(()=> res.data);
          setSetting(set => ({ ...set, loading: false }))

          if(data) {
            Router.push({
                pathname: '/invoice'
            })
            setCompanySettings(data)
            localStorage.save('company_settings', data);
            

            // get base currency
            const filter = data.filter(item => item.key === 'base.currency.id')[0];
            const idCurrency = getSafe(() => filter.value, '');
            axios.get(`/api/v2/currencies/${idCurrency}`)
            .then(res => {
                if(res.data) {
                    setCurrency(res.data)
                    localStorage.save('currency', res.data);
                }
            })
          }
        })
        .catch(err => {
            responseError(err)
            setSetting(set => ({ ...set, loading: false }))
        })
    }

    function migrateDatabase() {
        return new Promise( (resolve, reject) => {
            migrate().then(res => {
                const { data } = res
                if(data && data.version) {
                    const { current, last } = data.version
                    if ( current < last ) {
                        migrateDatabase()
                    } else {
                        // get data company settings
                        resolve(res)
                        fetchCompanySetting();
                    }
                }
            }).catch(err => {
                reject(err)
                responseError(err)
                setSetting(set => ({ ...set, loading: false }))
            })
        }) 
    }

    function getCompanySetting(key) {
        if( Array.isArray(companySettings) ) {
            const filter = companySettings.filter(item => item.key === key)[0];
            return getSafe(() => filter.value, '');
        } else {
            return '';
        }
    }

    const { children } = props;
    return(
        <UserContext.Provider 
            value={{ 
                user, 
                currency,
                setUser, 
                company, 
                saveCompany,
                logout,
                setting,
                setSetting,
                companySettings,
                setCompanySettings,
                saveUser,
                getCompanySetting,
                fetchCompanySetting
            }}>
            {children}
        </UserContext.Provider>
    )
}