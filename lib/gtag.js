export const GA_TRACKING_ID = 'UA-254411-21'

// https://developers.google.com/analytics/devguides/collection/gtagjs/pages
export const pageview = ({ title, path }) => {
  window.gtag('config', GA_TRACKING_ID, {
    page_title : title,
    page_path: path
  })
}

// https://developers.google.com/analytics/devguides/collection/gtagjs/events
export const event = ({ action, category, label, value }) => {
  window.gtag('event', action, {
    event_category: category,
    event_label: label,
    value: value
  })
}