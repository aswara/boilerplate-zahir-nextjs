import React, { Component } from 'react'
import Router from 'next/router'
import nextCookie from 'next-cookies'


export default function withAuth(AuthComponent) {
    return class Authenticated extends Component {

        static async getInitialProps(ctx) {
            // get token from cookies
            const { access_token, slug } = nextCookie(ctx);

            const redirectOnError = () =>
                typeof window !== 'undefined'
                    ? Router.push('/login')
                    : ctx.res.writeHead(302, { Location: '/login' });

            // redirect to login
            if(!access_token || !slug) {
                return redirectOnError();
            }

            // Ensures material-ui renders the correct css prefixes server-side
            let userAgent
            if (process.browser) {
                userAgent = navigator.userAgent
            } else {
                userAgent = ctx.req.headers['user-agent']
            }

            // Check if Page has a `getInitialProps`; if so, call it.
            const pageProps = AuthComponent.getInitialProps && await AuthComponent.getInitialProps(ctx);
            // Return props.
            return { ...pageProps, userAgent }
        }

        componentDidMount

        render() {
            return (
                <AuthComponent {...this.props} />
            )
        }
    }
}