import React from "react";
import {  useSnackbar } from 'notistack';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import { getSafe, isClient } from '../utils';

export default function useNotif() {

    const { enqueueSnackbar, closeSnackbar } = useSnackbar();


    function notifWarning(message) {
        enqueueSnackbar(message, { variant: "warning", action: key => (<IconButton style={{ color: 'white' }} onClick={()  => closeSnackbar(key)}><CloseIcon color="inherit" /></IconButton>) })
    }

    function notifError(message) {
        enqueueSnackbar(message, { variant: "error", action: key => (<IconButton style={{ color: 'white' }} onClick={()  => closeSnackbar(key)}><CloseIcon color="inherit" /></IconButton>) })
    }

    function notifSuccess(message) {
        enqueueSnackbar(message, { variant: "success", action: key => (<IconButton style={{ color: 'white' }} onClick={()  => closeSnackbar(key)}><CloseIcon color="inherit" /></IconButton>) })
    }

    function responseError(err) {
        const message = getSafe(() => err.response.data.message || err.response.data.error.message , 'Server error')
        enqueueSnackbar(message, { variant: 'error', action: key => (<IconButton style={{ color: 'white' }} onClick={()  => closeSnackbar(key)}><CloseIcon color="inherit" /></IconButton>) });
    }


    return {
        notifWarning,
        notifError,
        responseError,
        notifSuccess
    }
}