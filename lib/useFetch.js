import { useState } from 'react';
import axios from 'axios';
import { getSafe, isClient } from '../utils';
import useNotif from './useNotif';
import * as gtag from '../lib/gtag';
import version from '../version';
import { localStorage } from '../utils';

const prefix = '/api/v2/';

const endpoint = {
    user_companies: prefix + `user_companies`,
    sales_invoices: prefix + `sales_invoices`,
    products: prefix + `products`,
}

const useFetch = (module) => {
    const initialValues = {
        loadingGet: false, 
        loadingPost: false,
        loadingDetail: false,
        data: null,
    }

    const { responseError } =  useNotif();
    const [ response, setResponse] = useState(initialValues);
    const [ results, setResuts ] = useState([]);

    let url = endpoint[module] || module;

    function trackEvent(type) {
        const modulName = module ? module.toUpperCase() : ''
        const appName = 'SI';
        const company = localStorage.get('company')
        const companyName = getSafe(() => company.company.name, '')
        const companySlug = getSafe(() => company.company.slug, '')
        const companyStatus = getSafe(() => company.company.membership.status, '')
        const user = getSafe(() => company.email, '')

        gtag.event({
            category: `${appName} - ${modulName}`,
            action: `${appName} - ${modulName} - ${ type + '_' + modulName } - ${companyName}`,
            label: `${appName} - ${companyName} - ${companySlug} - ${user} - ${companyStatus}`
        })
    }

    function setLoading(key, value) {
        setResponse((data) => ({ ...data, [key]: value }));
    }

    function get(id, search, type) {
        return new Promise( (resolve, reject) => {
            if(id) {
                url = `${url}/${id}`
                setLoading('loadingDetail', true)
            } else {
                setLoading('loadingGet', true)
            }

            if(search) {
                if(url.includes('?')) {
                    url = `${url}&search[${search.by}]=${search.value}`
                } else {
                    url = `${url}?search[${search.by}]=${search.value}`
                }
            }

            axios.get(url)
            .then(res => {
                const result = getSafe(() => res.data.results ,[])
                setResponse((value) => ({ ...value, data: res.data, loadingGet: false, loadingDetail: false }))

                if(!id) {
                    setResuts(result)
                    trackEvent('LIST')
                }
                resolve(res)
            })
            .catch(err => {
                responseError(err)
                trackEvent('REJECTED')
                setLoading('loadingGet', false)       
                setLoading('loadingDetail', false)       
            })
        })
    }

    function autoLoad() {

        loadmore()
        const next = getSafe(() => response.data.links.next)
        if(next) {
            autoLoad()
        }
    }


    function loadmore() {
        const next = getSafe(() => response.data.links.next)
        if(next) {
            const params = next.split('/').slice(3);
            const url = params.join('/');
            setLoading('loadingGet', true)
            axios.get(url)
            .then(res => {
                setLoading('loadingGet', false)
                const result = getSafe(() => res.data.results ,[])
                setResponse((value) => ({ ...value, data: res.data, loadingGet: false }))                
                setResuts([...results, ...result])                
            })
            .catch(err => {
                setLoading('loadingGet', false)
                responseError(err)
            })
        }
    }

    function search(value, by = 'name') {
        return get(null, { by, value })
    }

    function post(body) {
        const loading = 'loadingPost'
        setLoading(loading, true)
        return new Promise( (resolve, reject) => {
            axios.post(url, body)
            .then(res => {
                setLoading(loading, false)                                  
                resolve(res)
                trackEvent('ADD')
            })
            .catch(err => {
                responseError(err)                
                setLoading(loading, false)
                reject(err)
            })
        })
    }

    function put(id, body) {
        const loading = 'loadingPost'
        setLoading(loading, true)
        return new Promise( (resolve, reject) => {
            axios.put(url + '/' + id, { ...body, reason: "update" } )
            .then(res => {
                setLoading(loading, false)                                  
                resolve(res)
                trackEvent('EDIT')
            })
            .catch(err => {
                responseError(err)                
                setLoading(loading, false)
                reject(err)
            })
        })
    }


    return {
        get,
        put,
        post,
        response,
        results,
        loadmore,
        search,
        autoLoad,
        setResuts,
        trackEvent
    }
};

export default useFetch;