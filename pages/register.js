import React,{ useState, useEffect } from 'react';
import { Formik } from 'formik';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

import Input from '../components/form/Input';
import WrapAuth from '../components/WrapAuth';
import { withTranslation } from '../i18n'
import * as Yup from 'yup';
import Link from 'next/link';
import Router from 'next/router';

import { checkEmail } from '../utils/api'; 
import { localStorage, getSafe } from '../utils';
import useNotif from '../lib/useNotif';


const Form = (props) => {

  const {
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    t,
    loading
  } = props;

  return (
    <form onSubmit={handleSubmit}>

      <Input
        label={t('email_address')}
        placeholder={t('type_yuor_email')}
        name="email"
        onChange={handleChange}
        onBlur={handleBlur}
        values={values}
        errors={errors}
        touched={touched}
        fullWidth
      />

      <Button
        size="large" 
        style={{ marginTop: 20, marginBottom: 20 }} 
        fullWidth 
        color="primary" 
        variant="contained" 
        type="submit" 
        >
        { loading ? <CircularProgress size={26} style={{ color:'white' }} /> : t('register') }
      </Button>
     
    </form>
  )
}

const Register = (props) => {
  const { t } = props;
  const [ loading, setLoading ] = useState(false);
  const [ initialEmail, setInitialEmail ] = useState('');
  const { responseError } = useNotif();
 
  // validation
  const RegisterSchema = Yup.object().shape({
    email: Yup.string()
      .email(t('invalid_email'))
      .required(t('required')),
  });

  useEffect(() => {
    const invoice = localStorage.get('invoice');

    if(invoice) {
      setInitialEmail(getSafe(() => invoice.company_email))
    }
  }, [])


  return(
    <WrapAuth>
      <div>
        <Typography align="center" variant="h4">{t('register')}</Typography>
        <Typography 
          style={{ marginTop: 10, marginBottom:30, color: '#9AACB7' }} 
          align="center" 
          variant="subtitle2"i
          >
          {t('register_desc')}
          </Typography>
        <Formik
          enableReinitialize
          initialValues={{ email: initialEmail }}
          validationSchema={RegisterSchema}
          onSubmit={(values, actions) => {
            setLoading(true)
            checkEmail(values.email)
            .then(res => {
              setLoading(false)

              const message = res.data.message;
              if(message === "email is not ready to use") {
                actions.setFieldError('email', t('email_already_use'))
              } else {      
                Router.push({
                  pathname: '/complete-profile',
                  query: { email: values.email },
                })
              }

            }).catch(err => {
              setLoading(false)
              responseError(err)
            })
          }}
          render={props => <Form loading={loading} t={t} {...props} />}
        />
        <Typography 
          style={{ marginTop: 0, marginBottom:30, color: '#9AACB7' }} 
          align="right" 
          variant="subtitle2"
          >
          {t('already_have_account')}. <Link href='/login'><span style={{ color: '#778CA3', cursor: 'pointer' }}>{t('get_in')}</span></Link>
        </Typography>
      </div>
    </WrapAuth>

)};

Register.getInitialProps = async (req) => {
  return { namespacesRequired: ['auth'] }
}

export default withTranslation('auth')(Register)