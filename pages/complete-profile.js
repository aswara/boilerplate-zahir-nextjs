import React, { useState, useEffect } from 'react';
import { Formik } from 'formik';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

import Input from '../components/form/Input';
import Select from '../components/form/Select';
import WrapAuth from '../components/WrapAuth';
import { withTranslation } from '../i18n'
import * as Yup from 'yup';
import Link from 'next/link';
import { useRouter } from 'next/router';

import { register } from '../utils/api';
import { getSafe, localStorage } from '../utils';
import currencies from '../utils/currencies';
import useNotif from '../lib/useNotif';
import useUser from '../lib/useUser';


const Form = (props) => {

  const {
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    t,
    loading,
    i18n,
    setFieldValue,
    setFieldTouched
  } = props;


  return (
    <form onSubmit={handleSubmit}>
      <div style={{ marginBottom: 20 }}>
        <Input
          placeholder={t('type_yuor_name')}
          name='name'
          onChange={handleChange}
          onBlur={handleBlur}
          values={values}
          errors={errors}
          touched={touched}
          fullWidth
        />
      </div>

      <div style={{ marginBottom: 20 }}>
        <Input
          placeholder={t('type_yuor_phone')}
          name='phone'
          onChange={handleChange}
          onBlur={handleBlur}
          values={values}
          errors={errors}
          touched={touched}
          fullWidth
        />
      </div>

      <div style={{ marginBottom: 20 }}>
        <Select
          onChange={option => setFieldValue('business', option.value)}
          onBlur={() => setFieldTouched('business')}
          name="business"
          value={values['business']}
          variant="filled"
          errors={errors}
          touched={touched}
          placeholder={t('business_type')}
          url={`/api/v2/businesses?type[language]=${i18n.language || 'en'}&consumer[$ilike]=%zo%&sort[sort_number]=1`}
        />
      </div>

      <div  style={{ marginBottom: 10 }}>
        <Select
          value={values['currency']}
          onChange={(v) => setFieldValue('currency', v)}
          options={currencies}
          keyLabel="label"
          variant="filled"
          name="currency"
          errors={errors}
          touched={touched}
          placeholder={t('currency')}
        />
      </div>

      <div style={{ marginBottom: 20 }}>
        <Input
          label={t('choose_your_password')}
          placeholder={t('type_yuor_password')}
          autoComplete="new-password"
          name='password'
          onChange={handleChange}
          onBlur={handleBlur}
          values={values}
          errors={errors}
          touched={touched}
          fullWidth
        />
      </div>

      <Button
        size="large"
        style={{ marginBottom: 20 }}
        fullWidth
        color="primary"
        variant="contained"
        type="submit"
      >
        {loading ? <CircularProgress size={26} style={{ color: 'white' }} /> : t('register')}
      </Button>

    </form>
  )
}

const Register = (props) => {
  const router = useRouter();
  const { email } = router.query;

  const { t, i18n } = props;
  const [loading, setLoading] = useState(false);
  const { responseError } = useNotif();
  const { saveCompany, saveUser } = useUser();

  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [address, setAddress] = useState('');
  const [city, setCity] = useState('');



  useEffect(() => {
    const invoice = localStorage.get('invoice');

    if (invoice) {
      setPhone(getSafe(() => invoice.company_phone), '');
      setName(getSafe(() => invoice.company_name), '');
      setAddress(getSafe(() => invoice.company_address), '');
      setCity(getSafe(() => invoice.company_city), '');
    }
  }, [])


  // validation
  const RegisterSchema = Yup.object().shape({
    name: Yup.string()
      .required(t('required')),
    password: Yup.string()
      .required(t('required'))
      .min(6, t('min_6')),
    business: Yup.object().shape({
      name: Yup.string()
        .required(t('required')),
    })

  });


  return (
    <WrapAuth>
      <div>
        <Typography align="center" variant="h4">{t('complete_profile')}</Typography>
        <Typography
          style={{ marginTop: 10, marginBottom: 30, color: '#9AACB7' }}
          align="center"
          variant="subtitle2"
        >
          {t('complete_profile_desc')}
        </Typography>
        <Formik
          enableReinitialize
          initialValues={{
            email,
            name,
            city,
            phone,
            address,
            currency: currencies.filter( item => item.code === 'IDR' )[0],
            password: '',
            business: '',
            language: i18n.language
          }}
          validationSchema={RegisterSchema}
          onSubmit={(values, actions) => {
            setLoading(true)
            register(values)
              .then(res => {

                setLoading(false)
                const user = getSafe(() => res.data.user);
                const company = getSafe(() => res.data.company);

                saveCompany(company);
                saveUser(user);

              }).catch(err => {
                setLoading(false)
                responseError(err)
              })
          }}
          render={props => <Form loading={loading} t={t} i18n={i18n} {...props} />}
        />
        <Typography
          style={{ marginTop: 0, marginBottom: 30, color: '#9AACB7' }}
          align="right"
          variant="subtitle2"
        >
          {t('already_have_account')}. <Link href='/login'><span style={{ color: '#778CA3', cursor: 'pointer' }}>{t('get_in')}</span></Link>
        </Typography>
      </div>
    </WrapAuth>

  )
};

Register.getInitialProps = async (req) => {
  return { namespacesRequired: ['auth'] }
}

export default withTranslation('auth')(Register)