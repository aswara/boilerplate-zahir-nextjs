import React, { useState } from 'react';
import { Formik } from 'formik';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

import Input from '../components/form/Input';
import WrapAuth from '../components/WrapAuth';
import { withTranslation } from '../i18n'
import * as Yup from 'yup';
import Link from 'next/link';
import Router from 'next/router';

import { checkEmail, login } from '../utils/api';
import { getSafe } from '../utils';

import useNotif from '../lib/useNotif';
import useUser from '../lib/useUser';
import useFetch from '../lib/useFetch';


const Form = (props) => {

  const {
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    t,
    loading
  } = props;

  return (
    <form onSubmit={handleSubmit}>

      <div style={{ marginBottom: 20 }}>
        <Input
          label={t('email_address')}
          placeholder={t('type_yuor_email')}
          name="email"
          onChange={handleChange}
          onBlur={handleBlur}
          values={values}
          errors={errors}
          touched={touched}
          fullWidth
        />
      </div>

      <div style={{ marginBottom: 20 }}>
        <Input
          label={t('password')}
          placeholder={t('type_yuor_password')}
          name='password'
          onChange={handleChange}
          onBlur={handleBlur}
          values={values}
          errors={errors}
          touched={touched}
          fullWidth
        />
      </div>

      <Button
        size="large"
        style={{ marginBottom: 20 }}
        fullWidth
        color="primary"
        variant="contained"
        type="submit"
      >
        {loading ? <CircularProgress size={26} style={{ color: 'white' }} /> : t('login')}
      </Button>

    </form>
  )
}

const Login = (props) => {
  const { t } = props;
  const [loading, setLoading] = useState(false);
  const { responseError } = useNotif();
  const { user, saveUser, saveCompany } = useUser();
  const { get: getUserCompanies } = useFetch('user_companies');

  // validation
  const RegisterSchema = Yup.object().shape({
    email: Yup.string()
      .email(t('invalid_email'))
      .required(t('required')),
    password: Yup.string()
      .required(t('required')),
  });

  
  function handleSubmit(values, actions) {
    setLoading(true)
    checkEmail(values.email)
      .then(res => {
        setLoading(false)

        const message = res.data.message;
        if (message === 'email is ready to use') {
          actions.setFieldError('email', t('email_not_registered'))
        } else {
          login(values)
            .then(res => {
              const data = res.data;
              if (data) {
                // set data user to global
                saveUser(data);

                // get data company user
                getUserCompanies()
                  .then(res => {
                    const results = getSafe(() => res.data.results);

                    // condition results companies
                    if (results.length === 1) {
                      const company = results[0];
                      saveCompany(company);
                    } else {
                      Router.push({
                        pathname: '/companies'
                      })
                    }

                  })
              }
            })
            .catch(err => {
              actions.setFieldError('password', t('password_wrong'))
              responseError(err)
            })
        }
      }).catch(err => {
        setLoading(false)
        responseError(err)
      })
  }

  return (
    <WrapAuth>
      <div>
        <Typography align="center" variant="h4">{t('login')}</Typography>
        <Typography
          style={{ marginTop: 10, marginBottom: 30, color: '#9AACB7' }}
          align="center"
          variant="subtitle2" i
        >
          {t('login_desc')}
        </Typography>
        <Formik
          initialValues={{ email: '', password: '' }}
          validationSchema={RegisterSchema}
          onSubmit={handleSubmit}
          render={props => <Form loading={loading} t={t} {...props} />}
        />
        <Typography
          style={{ marginTop: 0, marginBottom: 30, color: '#9AACB7' }}
          align="right"
          variant="subtitle2"
        >
          {t('dont_have_account')}. <Link href='/register'><span style={{ color: '#778CA3', cursor: 'pointer' }}>{t('register')}</span></Link>
        </Typography>
      </div>
    </WrapAuth>

  )
};

Login.getInitialProps = async (req) => {
  return { namespacesRequired: ['auth'] }
}

export default withTranslation('auth')(Login)