import React, { useState, useEffect } from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Search from '@material-ui/icons/Search';
import Refresh from '@material-ui/icons/Refresh';
import Router from 'next/router';
import moment from 'moment';

import WrapAuth from '../components/WrapAuth';
import { withTranslation } from '../i18n'

import { getSafe, isClient } from '../utils';
import { checkEmail, login } from '../utils/api';
import useNotif from '../lib/useNotif';
import useUser from '../lib/useUser';
import useFetch from '../lib/useFetch';

import Table from '../components/Table';
import Footer from '../components/Footer';


import { Grid } from '@material-ui/core';

const statusCompany = (status) => {
  let background = '#20BF6B';

  switch (status) {

    case 'Expired':
      background = '#EB3B5A'
      break;
    case 'Trial':
      background = '#F7B731'
      break;
    case 'Awaiting Payment':
      background = '#2D98DA'
      break;
    case 'Post Trial':
      background = '#FA8231'
      break;
    case 'Subscribe':
        background = '#20BF6B'
        break;
    default:
      background = '#A5B1C2'
      break;
  }

  return (
    <div style={{
      fontSize: 12,
      background,
      width: 60,
      padding: 2,
      borderRadius: 3,
      textAlign: 'center',
      color: 'white'
    }}>
      {status}
    </div>
  )
}

const Login = (props) => {
  const { t } = props;
  ;
  const { responseError } = useNotif();
  const { company, saveCompany } = useUser();
  const { response, get, results, loadmore, search } = useFetch('user_companies');
  const [searchValue, setSearchValue] = useState('')

  const { get: getCompanySettings } = useFetch(`/api/v2/company_settings?is_skip_pagination=true`);

  useEffect(() => {
    get();
  }, [])

  // rebuild table data
  const data = results && results.map(item => ({
    ...item,
    name: getSafe(() => item.company.name, ''),
    edition: getSafe(() => item.company.membership.variant.name, ''),
    expired: getSafe(() => item.company.membership.expired_date) ? moment(getSafe(() => item.company.membership.expired_date)).format('D-MMM-YYYY') : '',
    status: statusCompany(getSafe(() => item.company.membership.status, '')),
  }))

  function handleSelect(value) {
    delete value.status
    saveCompany(value);
  }


  let timeout = null

  function actionSearch(val) {
    if (timeout) clearTimeout(timeout);
    timeout = setTimeout(() => {
      if (val === '') {
        get();
      } else {
        search(val, 'company.name')
      }
    }, 500);
  }

  function handleSearch(e) {
    const value = e.target.value;
    setSearchValue(value)
    actionSearch(value)
  }

  function handleRefresh() {
    setSearchValue('');
    get();
  }

  const height = isClient ? window.innerHeight - 250 : 460;

  return (
    <div>
      <WrapAuth full>
        <div className="wrap-table">
          <Grid container spacing={2}>
            <Grid item md={6} xs={12}>
              <Typography variant="h5">{t('choose_company')}</Typography>
              <Typography variant="body1">{t('desc_choose_company')}</Typography>
            </Grid>
            <Grid item md={6} xs={12}>
              <Grid container justify="flex-end">
                <IconButton
                  size="small"
                  style={{ marginRight: 10, background: '#F5F7F8', width: 40, borderRadius: 5 }}
                  onClick={handleRefresh}
                >
                  <Refresh color="primary" />
                </IconButton>
                <TextField
                  value={searchValue}
                  onChange={handleSearch}
                  variant="filled"
                  placeholder={t('search')}
                  InputProps={{
                    style: { padding: 1, height: 40 },
                    disableUnderline: true,
                    startAdornment: <>{
                      <InputAdornment style={{ marginTop: 0, marginLeft: 10 }} position="start">
                        <Search color="primary" />
                      </InputAdornment>
                    }</>
                  }}
                />
              </Grid>

            </Grid>
          </Grid>
          <div style={{ marginTop: 20 }}></div>
          <Table
            height={height}
            onSelect={handleSelect}
            data={data}
            loadmore={loadmore}
            response={response}
            columns={[
              {
                width: 220,
                label: t('company'),
                dataKey: 'name',
              },
              {
                width: 120,
                label: t('edition'),
                dataKey: 'edition',
              },
              {
                width: 120,
                label: t('expired'),
                dataKey: 'expired',
                numeric: true,
                flexGrow: 1
              },
              {
                width: 120,
                label: t('status'),
                dataKey: 'status',
              }
            ]}
          />
        </div>
      </WrapAuth>
      <style jsx global>{`
        .wrap-table {
          padding-top: 20px;
          background: white;
        }
        body { 
          padding: 0;
          margin: 0;
          color: #394D6F;
      }
      footer {
        padding: 10px !important;
      }
      `}</style>
    </div>
  )
};

Login.getInitialProps = async (req) => {
  return { namespacesRequired: ['common'] }
}

export default withTranslation('common')(Login)